# Postman Testing GitLab CI/CD

This repository contains the configuration and scripts for running Postman tests as part of a GitLab CI/CD pipeline.

## Prerequisites

Before running the tests, make sure you have the following installed:

- [Postman](https://www.postman.com/downloads/)
- [Newman](https://www.npmjs.com/package/newman)
- [GitLab Runner](https://docs.gitlab.com/runner/install/)

## Getting Started

1. Clone this repository to your local machine:

```shell
git clone <repository-url>
```

2. Install the required dependencies:

```shell
npm install
```

3. Configure your environment variables:

- Open the `.env` file and update the necessary variables with your own values.

4. Run the api:

```shell
npx ts-node src/app.ts
```

## CI/CD Pipeline

This repository is configured with a GitLab CI/CD pipeline that automatically runs the Postman tests whenever changes are pushed to the repository. The pipeline is defined in the `.gitlab-ci.yml` file.

## Contributing

If you would like to contribute to this project, please follow these steps:

1. Fork the repository.
2. Create a new branch for your feature or bug fix.
3. Make your changes and commit them.
4. Push your changes to your forked repository.
5. Open a pull request in this repository.

## License

This project is licensed under the [MIT License](LICENSE).
