## Mise en place de tests Postman sur une API NodeJS

1. Créez un repository gitlab
2. Clonez le repository en local
3. Créez un répertoire src qui contiendra l’application
4. Créez un fichier app.js dans src
5. Créez un fichier `📄 package.json` tel que :

```JSON
{
"name": "tp-nodejs",
"version": "1.0.0",
"description": "",
"main": "src/app.js",
"scripts": {
"test": "node src/app.js"
  },
  "author": "Lilian Calliot",
  "license": "ISC",
  "dependencies": {
  "body-parser": "^1.20.1",
  "express": "^4.17.1"
  }
}
```

6. Exécutez la commande `npm install` depuis la racine du répertoire projet
7. Importez la dépendance express et instanciez-la dans le app.js tel que

```js
var express = require("express");
var app = express();
```

8. Créez une route `/status` dans le app.js qui renvoie un code 200 tel que

```js
// TDV
app.get("/status", function (request, response) {
  response.status(200).send();
  response.end();
});
```

9. Faite écouter votre API sur le port 3000 tel que

```js
var port = process.env.PORT || 3000;
app.listen(port);
```

**10. Testez votre API**

Result should be :

```bash
curl -I http://localhost:3000/status
HTTP/1.1 200 OK
X-Powered-By: Express
Date: Thu, 10 Nov 2022 16:20:45 GMT
Connection: keep-alive
```

11. Ajoutez une route par défaut qui renvoie une erreur 404

```js
//Capture All 404 errors
app.use(function (req, res, next) {
  res.status(404).send("Unable to find the requested resource!");
  res.end();
});
```

**12. Testez votre API**

Result should be :

```bash
curl -I http://localhost:3000/lilian
HTTP/1.1 404 Not Found
X-Powered-By: Express
Content-Type: text/html; charset=utf-8
Content-Length: 38
ETag: W/"26-ZUXxu7Kod7bZhBhcI7dEsNiAw48"
Date: Thu, 10 Nov 2022 16:06:38 GMT
Connection: keep-alive
```

13. Ajoutez une route /city qui renvoie une liste de villes

```js
// CITY
app.get("/city", function (request, response) {
  const city = [
    "Paris",
    "Bordeaux",
    "Lyon",
    "Strasbourg",
    "Toulouse",
    "Marseille",
  ];
  response.json(city);
  response.end();
});
```

**14. Testez votre API**

Result should be :

```bash
curl -I http://localhost:3000/status
HTTP/1.1 200 OK
X-Powered-By: Express
Date: Thu, 10 Nov 2022 16:20:45 GMT
Connection: keep-alive
```
